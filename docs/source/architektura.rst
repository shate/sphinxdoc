Architektura
==============

Diagram klas
-------------


.. uml::
        
    class Customer {
	-id : Long
	-firstName : String
	-lastName : String
	+toString() : String
    }

    interface CustomerRepository
    Customer --> CustomerRepository

    CustomerRepository --> CustomerService

    class CustomerService {
	-customerRepository : CustomerRepository
	+findAll() : Iterable<Customer>
	+findById(Long) : Customer
    }

    CustomerService --> CustomerController

    class CustomerController {
	-customerService : CustomerService
	+getCustomers() : Iterable<Customer>
	+getCustomerById(Long) : Customer
    }

Diagram przypadkow uzycia
--------------------------
Jest to wyjatkowo prosta aplikacja, w ktorej istnieja dwa endpointy: **/customers** zwracajacy wszystkich klientow oraz **/customers/{id}** zwracajacy klienta o zadanym id. Skutkuje to prostota diagramu przypadkow uzycia, w ktorym jedynym "aktorem" jest gosc (aplikacja nie posiada zadnego systemu logowania), a przypadki uzycia to pokazanie danych wszystkich klientow oraz pokazanie danych konkretnego klienta. Kazdy z tych przypadkow wymaga pobrania danych z bazy.

.. uml::

	left to right direction
	skinparam packageStyle rectangle
	actor guest
	actor database
	rectangle customerSystem {
	  guest --> (show all customers)
	  guest --> (show customer)
	  (show all customers) .> (get data) : include
	  (show customer) .> (get data) : include
	  (get data) -- database
	}




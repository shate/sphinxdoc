Wprowadzenie
===================================================

Docker
--------
Docker jest platforma sluzaca do tworzenia, udostepniania i uruchamiania aplikacji. Dzieki dockerowi praca w srodowisku developerskim staje sie o wiele prostsza. Korzystajac z dockera pomijamy instalacje serwisow, z ktorych korzysta aplikacja, poniewaz docker wykonuje to za nas w stworzonej przez siebie maszynie wirtualnej. Umozliwia to znaczny wzrost efektywnosci tworzenia aplikacji z innymi programistami, poniewaz wszyscy pracuja na tych samych ustawieniach i kazdy posiada te same wersje uzywanych aplikacji.



Docker compose
---------------
Docker compose jest narzedziem sluzacym do uruchamiania wielu kontenerow swtorzonych przez dockera. Za pomoca dockera tworzymy dla kazdego mikroserwisu w naszej aplikacji kontener, a nastepnie dzieki docker compose integrujemy wszystkie te kontenery w jedna calosc, aby stworzyc w pelni funkcjonalna aplikacje. Docker compose jest poteznym narzedziem ze wzgledu na jego prostote, a mianowicie wystarczy stworzyc plik YAML (w ktorym deklarujemy kontenery) i kilka dodatkowych plikow konfiguracyjnych, a nastepnie uruchomic to wszystko pojedyncza komenda. Dzieki docker compose developer moze przeznaczyc wiecej czasu na tworzenie aplikacji (nie marnuje tego czasu na konfiguracje, ktore wystarczy napisac raz a nastepnie uruchamiac je wielokrotnie na roznych maszynach).

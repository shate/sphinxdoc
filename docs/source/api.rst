REST api
===========


Endpointy
-----------

1. **/customers** - Zwraca liste wszystkich klientow
|customers|

2. **/customers/{id}** - Zwraca klienta o zadanym ID.

|customersId|


.. |customers| image:: _static/customers.png
.. |customersId| image:: _static/customersId.png

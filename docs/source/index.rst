.. lab3 documentation master file, created by
   sphinx-quickstart on Wed May 20 12:32:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Inżynieria oprogramowania - Dokumentacja
=========================================

.. toctree::
    :maxdepth: 2
    :caption: Zawartość

    wprowadzenie
    instalacja
    architektura
    api
    


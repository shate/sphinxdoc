Instalacja
===================================================

Wymagania
-----------
 
`Docker <https://docs.docker.com/engine/install/ubuntu/>`_

`Docker compose <https://docs.docker.com/compose/install/>`_

Pliki
------
- Do stworzenia pojedynczego kontenera niezbedny jest plik **Dockerfile** w ktorym krok po kroku instruujemy dockera co ma zrobic z naszym serwisem.
- W celu ujednolicenia wszystkich metadanych tworzy sie plik **.env**, ktory przechowuje wszystkie zmienne srodowiskowe, z ktorych korzystamy podczas uruchamiania aplikacji.
- Jezeli dla przykladu korzystamy z bazy danych, to niezbedne rowniez bedzie utworzenie pliku bashowego, w ktorym takowa baze (wraz z uzytkownikiem) stworzymy. Plik taki nosi nazwe **create-databases.sh** i znajduje sie w folderze **docker-entrypoint-initdb.d**, ktory lezy w glownym katalogu projektu.
- Jesli korzystamy z frameworka typu `Spring <https://spring.io/>`_, to konieczne jest rowniez uzupelnienie pliku **application.properties**
- Ostatnim elementem jest plik **docker-compose.yml**, w ktorym deklarujemy wszystkie kontenery, z ktorych sklada sie nasza aplikacja i wypelniamy niezbedne do uruchomienia kontenerow zmienne srodowiskowe.



Dockerfile
^^^^^^^^^^^
Przykladowa struktura pliku **Dockerfile**:

.. code-block:: bash

    	FROM gradle:6.4.0-jdk8 AS build
	USER root
	RUN mkdir app
	COPY src/ /app/src/
	COPY build.gradle /app/
	COPY settings.gradle /app/
	WORKDIR /app
	RUN gradle bootJar

	FROM openjdk:8-jdk-alpine
	COPY --from=build /app/build/libs/app.jar app.jar
	ENTRYPOINT ["java","-jar","/app.jar"] 

.env
^^^^^
Przykladowa struktura pliku **.env**:

.. code-block:: bash

	POSTGRES_USER=user
	POSTGRES_PASSWORD=password
	DB_PORT=5433

create-databases.sh
^^^^^^^^^^^^^^^^^^^^
Przykladowa stuktura pliku **create-databases.sh**:

.. code-block:: bash

	#!/bin/bash
	set -e

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	    CREATE USER docker;
	    CREATE DATABASE docker;
	    GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
	EOSQL


docker-compose.yml
^^^^^^^^^^^^^^^^^^^
Przykladowa struktura pliku **docker-compose.yml**:

.. code-block:: yaml

	version: "3"
	services:
	  app:
	    image: "app/customer"
	    build:
	      context: .
	      dockerfile: "Dockerfile"
	    environment:
	      POSTGRES_USER: ${POSTGRES_USER}
	      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
	    ports:
	      - 8080:8080
	  db:
	    image: postgres:latest
	    volumes:
	      - "./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d"
	    environment:
	      POSTGRES_USER: ${POSTGRES_USER}
	      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
	    ports:
	      - ${DB_PORT}:5432




Uruchamianie
--------------

1. Z poziomu katalogu nadrzednego budujemy obrazy:

.. code-block:: bash

	docker-compose build

2. Z poziomu katalogu nadrzednego uruchamiamy kontenery:

.. code-block:: bash

	docker-compose up

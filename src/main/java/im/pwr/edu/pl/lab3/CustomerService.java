package im.pwr.edu.pl.lab3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Iterable<Customer> findAll(){
        return customerRepository.findAll();
    }

    public Customer findById(Long customerId){
        return customerRepository.findById(customerId).orElseThrow(()->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found"));
    }
}
